# Install:

`npm i`

# Start:

`npm run dev`

Runs on http://localhost:3000/

# Export to static HTML:

`npm run export`
